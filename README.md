docker image that allows to reuse the hosts nix store.

Sample usage: `podman run --rm -it -v .:/workspace -v /nix/store:/nix/store:ro -v /nix/var/nix/db:/nix/var/nix/db:ro -v /nix/var/nix/daemon-socket:/nix/var/nix/daemon-socket:ro --env NIX_REMOTE=daemon --env PATH=/nix/var/nix/profiles/default/bin:/nix/var/nix/profiles/default/sbin:/bin nix-flake-docker`

Push: `skopeo copy $"docker-archive://(readlink result | str trim)" docker://git.disroot.org/liketechnik/nix-flake-docker:latest` (after nix build)
