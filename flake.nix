{
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

  inputs.docker-nixpkgs = {
    url = "github:nix-community/docker-nixpkgs";
    flake = false;
  };

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = {
    nixpkgs,
    flake-utils,
    docker-nixpkgs,
    ...
  } @ inputs:
    flake-utils.lib.eachSystem flake-utils.lib.allSystems (
      system: let
        pkgs = import nixpkgs {inherit system;};
        gitReallyMinimal =
          (pkgs.pkgsStatic.git.override {
            perlSupport = false;
            pythonSupport = false;
            withManual = false;
            withpcre2 = false;
          })
          .overrideAttrs (
            _: {
              # installCheck is broken when perl is disabled
              doInstallCheck = false;
            }
          );
      in rec {
        formatter = pkgs.alejandra;

        packages.default = pkgs.callPackage "${docker-nixpkgs}/images/nix-unstable-static" {
          dockerTools =
            pkgs.dockerTools
            // {
              buildImage = args:
              # intentionally swap out buildImage with buildLayeredImage,
              # otherwise podman load is unhappy
                pkgs.dockerTools.buildLayeredImage (args
                  // {
                    extraCommands =
                      args.extraCommands
                      + ''
                        mkdir -pv -m 0755 nix/var/log/nix/drvs
                        mkdir -pv -m 0755 nix/var/nix/gcroots
                        mkdir -pv -m 0755 nix/var/nix/profiles
                        mkdir -pv -m 0755 nix/var/nix/temproots
                        mkdir -pv -m 0755 nix/var/nix/userpool
                        mkdir -pv -m 1777 nix/var/nix/gcroots/per-user
                        mkdir -pv -m 1777 nix/var/nix/profiles/per-user
                        mkdir -pv -m 0755 nix/var/nix/profiles/per-user/root

                        cp -a ${gitReallyMinimal}/bin/* bin/
                        rm_ref -t ${gitReallyMinimal} bin/git
                      '';

                    config = pkgs.lib.attrsets.recursiveUpdate args.config {
                      Env =
                        pkgs.lib.lists.remove "PATH=/usr/bin:/bin" args.config.Env
                        ++ [
                          "PATH=/nix/var/nix/profiles/default/bin:/nix/var/nix/profiles/default/sbin:/usr/bin:/bin"
                          "NIX_REMOTE=daemon"
                        ];
                    };
                  });
            };
        };
      }
    );
}
